package com.noparanoia.tbs;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

public class Player {

    private Bitmap bitmap;
    private Bitmap scaledBitmap;

    private int x;
    private int y;

    private int width;
    private int height;
    private int spriteSize;

    private Paint paint;

    public Player(Context context, double xScaling, double yScaling) {
        x = 75;
        y = 50;

        spriteSize = 32 * (int) xScaling;
        width = 64 * (int) xScaling;
        height = 47 * (int) yScaling;

        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.player);
        scaledBitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
    }

    public void update() {

    }

    public void setPlayerX(int newX) {
        x = newX;
    }

    public int getPlayerX() {
        return x;
    }

    public void setPlayerY(int newY) {
        y = newY;
    }

    public int getPlayerY() {
        return y;
    }

    public int getPlayerWidth() {
        return width;
    }

    public int getPlayerHeight() {
        return height;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(
                scaledBitmap,
                getPlayerX(),
                getPlayerY(),
                paint
        );
    }
}
