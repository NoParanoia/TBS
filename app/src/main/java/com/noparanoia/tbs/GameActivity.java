package com.noparanoia.tbs;

import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.widget.Toast;

public class GameActivity extends AppCompatActivity {

    private GameView gameView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Forcing landscape orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //getting display object
        Display display = getWindowManager().getDefaultDisplay();

        //getting screen resolution
        Point size = new Point();
        display.getSize(size);

        Toast.makeText(this, Integer.toString(size.x) + " x " + Integer.toString(size.y), Toast.LENGTH_SHORT).show();

        gameView = new GameView(this, size.x, size.y);

        setContentView(gameView);
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameView.resume();
    }
}
