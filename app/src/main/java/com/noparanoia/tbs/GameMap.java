package com.noparanoia.tbs;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

public class GameMap {
    private int x;
    private int y;
    private int tileX;
    private int tileY;
    private int width;
    private int height;
    private int tileSize;

    private Bitmap bitmap1;
    private Bitmap bitmap2;
    private Bitmap scaledBitmap1;
    private Bitmap scaledBitmap2;

    private Paint paint;

    private int levelArr[][] = {    {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                                    {1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1},
                                    {1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1},
                                    {1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1},
                                    {1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1},
                                    {1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1},
                                    {1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1},
                                    {1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1},
                                    {1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1},
                                    {1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1},
                                    {1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1},
                                    {1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1},
                                    {1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1},
                                    {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
                                };


    public GameMap(Context context, double xScaling, double yScaling) {
        x = 0;
        y = 0;

        tileSize = 32 * (int) xScaling;
        width = 64 * (int) xScaling;
        height = 32 * (int) yScaling;

        bitmap1 = BitmapFactory.decodeResource(context.getResources(), R.drawable.tile1);
        scaledBitmap1 = Bitmap.createScaledBitmap(bitmap1, width, height, true);

        //bitmap2 = BitmapFactory.decodeResource(context.getResources(), R.drawable.tile2);
        //scaledBitmap2 = Bitmap.createScaledBitmap(bitmap2, width, height, true);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }


    public void draw(Canvas canvas) {
        for (int i = 0; i < levelArr[0].length; i++) {
            for (int j = 0; j < levelArr.length; j++) {
                tileX = j * tileSize;
                tileY = i * tileSize;
                canvas.drawBitmap(
                        scaledBitmap1,
                        // Convert to isometric coordinates
                        tileX - tileY,
                        (tileX + tileY) /2,
                        paint
                );
            }
        }
    }
}


