package com.noparanoia.tbs;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements Runnable{

    //to track if the game is playing
    volatile boolean playing;

    volatile int vSizeX;
    volatile int vSizeY;

    private double xScaling;
    private double yScaling;

    private Thread gameThread = null;

    private Player player;
    private GameMap map;

    // below is used for drawing
    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;

    public GameView(Context context, int screenX, int screenY) {
        super(context);

        vSizeX = 640;
        vSizeY = 320;

        xScaling = screenX/vSizeX;
        yScaling = screenY/vSizeY;

        //init Player object
        player = new Player(context, xScaling, xScaling);
        //init gameMap object
        map = new GameMap(context, xScaling, yScaling);

        //init drawing objects
        surfaceHolder = getHolder();
        paint = new Paint();
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:

                break;
            case MotionEvent.ACTION_DOWN:
                //player.setBoosting();
                player.setPlayerX(((int) (motionEvent.getX() / 64) * 64) - (player.getPlayerWidth()/2));
                player.setPlayerY(((int) (motionEvent.getY() / 32) * 32)- (player.getPlayerHeight()/2));
                break;
        }
        return true;
    }

    @Override
    public void run() {
        while (playing) {
            update();
            draw();
            control();
        }

    }

    private void update() {
        //update player
        player.update();
    }

    private void draw() {
        if(surfaceHolder.getSurface().isValid()) {
            canvas = surfaceHolder.lockCanvas();
            canvas.drawColor(Color.BLACK);
            map.draw(canvas);
            player.draw(canvas);
            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    private void control() {
        try {
            gameThread.sleep(17);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void pause() {
        playing = false;
        try {
            gameThread.join();
        } catch (InterruptedException e){
        }
    }

    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }
}
